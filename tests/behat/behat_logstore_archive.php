<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Steps definitions related with the standard log archive log store.
 *
 * @package    logstore_archive
 * @category   test
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(__DIR__ . '/../../../../../../../lib/behat/behat_base.php');
use Behat\Behat\Context\Step\Given,
    Behat\Gherkin\Node\TableNode,
    Behat\Behat\Exception\PendingException;

/**
 * Standard log archive related steps definitions.
 *
 * @package    logstore_archive
 * @category   test
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class behat_logstore_archive extends behat_base {
    /**
     * Enable the Standard log archive in Moodle.
     *
     * @Given /^Standard log archive is enabled$/
     */
    public function standard_log_archive_is_enabled() {
        set_config('enabled_stores', 'logstore_standard,logstore_archive', 'tool_log');
    }
}
