@logstore @logstore_archive @uon
Feature: Manage standard log archive settings
    In order to maintain the log store
    As an administrator
    I need to be able to configure the settings on the log store.

    Scenario: Change the log store settings.
        Given Standard log archive is enabled
        And I log in as "admin"
        And I navigate to "Plugins > Logging > Standard log archive" in site administration
        And I set the following fields to these values:
            | Choose database driver | Improved MySQL (native/mysqli) |
            | Database host | example.com |
            | Database user | moodle |
            | Database password | p@ssw0rd |
            | Database name | archive |
            | Database table | archive_logstore_standard_log |
            | Persistent database connections | 1 |
            | Unix socket | /var/run/mysqld/mysqld.sock |
            | Database port | 667 |
            | Database schema | archiveschema |
            | Database collation | utf-8 |
            | id_s_logstore_archive_archiveafterv | 179 |
            | id_s_logstore_archive_archiveafteru | days |
            | id_s_logstore_archive_archivelifetimev | 730 |
            | id_s_logstore_archive_archivelifetimeu | days |
        When I press "Save changes"
        And I wait to be redirected
        And I navigate to "Plugins > Logging > Standard log archive" in site administration
        Then the following fields match these values:
            | Choose database driver | Improved MySQL (native/mysqli) |
            | Database host | example.com |
            | Database user | moodle |
            | Database password | p@ssw0rd |
            | Database name | archive |
            | Database table | archive_logstore_standard_log |
            | Persistent database connections | 1 |
            | Unix socket | /var/run/mysqld/mysqld.sock |
            | Database port | 667 |
            | Database schema | archiveschema |
            | Database collation | utf-8 |
            | id_s_logstore_archive_archiveafterv | 179 |
            | id_s_logstore_archive_archiveafteru | days |
            | id_s_logstore_archive_archivelifetimev | 730 |
            | id_s_logstore_archive_archivelifetimeu | days |
